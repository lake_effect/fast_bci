Intermediate Report (AUTD data)
==========================

### Preparing Data

First, let's load some necessary libraries and choose a specific subject we are interested in.

```{r load.libs, results='hide', message=FALSE}
library(data.table) # efficient data structure
library(foreach) # parallel loops

setwd(project.root)
source("./.Rprofile")


this.subject <- "s01"

# load unfiltered data
# load("./data/AUTD/au/autd.Rdata") 
# this.erp <- erp[Subject == this.subject,]

# # load filtered data
load("./data/AUTD/au/autd_filtered.Rdata")
setkey(erp.filtered,Subject)
this.erp <- erp.filtered[this.subject,]
```

Initially, all of the data is stored in one huge `data.table` of size `r my.size <- print(object.size(erp.filtered),units="GB"); my.size` bytes (for the 10Hz low-pass filtered+downsampled table), so this also saves us a little time trying to query the larger table.

Next, we subtract the baseline average from each of the signals. This function averages the `Voltage` column up to `Time=0`, grouped by `Channel` and subtracts it from the same, adding a `Baseline` column in case it needs to be used later.
``` {r prestim.baseline}
this.erp <- baseline.remove(this.erp)
```

As a quick check, we plot each of the channels split by `Class`, averaged over `Trial`:

``` {r class.avgs}
survey.plot(this.erp,
            title.s = "Averaged ERP by Class, Channel")
```

This looks okay, so we can move on to preparing the data for classification.

``` {r relabel.classes, echo=FALSE, eval=FALSE}
# Relabel classes to {1, 2}
# this.erp[,Class:= as.numeric(Class)]
# this.erp[(Class==1),Class:=2]
# this.erp[(Class==0),Class:=1]
# this.erp[,Class:= as.factor(Class)]

# Relabel classes to {-1,1}
this.erp[,Class:= as.numeric(Class)]
this.erp[(Class==0),Class:=-1]
this.erp[,Class:= as.factor(Class)]

# Relabel classes to {0,1}
# this.erp[,Class:= as.numeric(Class)]
# this.erp[(Class==1),Class:=0]
# this.erp[(Class==2),Class:=1]
# this.erp[,Class:= as.factor(Class)]

```

```{r set.splitting, eval=FALSE, echo=FALSE}
## Splitting that ignores session index.

# Scale features
this.erp[,Voltage:= scale(Voltage)]

# Time subset
time.s <- 200
time.e <- 350

# Channel subset
chan.subset <- c(9,11)
# chan.subset <- this.erp[,unique(Channel)]

# Training set size
sample.fraction <- 0.7
trial.sets <- simple.sampler(this.erp,equal.class=TRUE)

this.erp.training <- trial.sets$training.table[(Time >= time.s & 
                                                Time <= time.e &
                                                Channel %in% chan.subset),]
this.erp.testing <- trial.sets$testing.table[(Time >= time.s & 
                                              Time <= time.e &
                                              Channel %in% chan.subset),]

# Also produce channel-wise columnar tables for some learning algorithms
this.erp.training.channel <- channel.form(this.erp.training,has.dups = TRUE)
this.erp.testing.channel <- channel.form(this.erp.testing, has.dups = TRUE)
```


### Preprocessing

First, we scale all of the `Voltage` measurements so that they have mean 0 and variance 1 (barring numeric error). Next, we choose two out of the three sessions for training, and the remaining one for testing.

```{r session.splitting}
# Scale features
this.erp[,Voltage:= scale(Voltage)]

# train on two sessions, test on one
training.sessions <- c(1,2)
testing.sessions <- 3
setkey(this.erp,Session)
this.erp.training <- this.erp[J(as.factor(training.sessions))]
this.erp.testing <- this.erp[J(as.factor(testing.sessions))]

# this.erp.training <- trial.unit.averager(this.erp.training)

trial.sets <- simple.sampler(this.erp.training,equal.class=TRUE,train.size=1)

```

The `simple.sampler` function above, as called above, randomly (and uniformly) samples the given table to create training and testing sets. Since we want minimum bias in the training set (equal class proportions), we allow it to sample the table with replacement (since the proportions are so different). Additionally, we know that the whole training table is to be used, so we explicitly tell the function to sample over the whole set.

Let's inspect the two sets using the same averaged plotting scheme as before.

``` {r split.class.avgs}
survey.plot(this.erp.training,
            title.s = "Averaged ERP by Class, Channel (training set)")

survey.plot(this.erp.testing,
            title.s = "Averaged ERP by Class, Channel (testing set)")
```


```{r subsetting}
# Time subset
time.s <- 200
time.e <- 450

# Channel subset
# chan.subset <- c(6,9)
chan.subset <- this.erp[,unique(Channel)]

this.erp.training <- trial.sets$training.table[(Time >= time.s & 
                                                Time <= time.e &
                                                Channel %in% chan.subset),]
this.erp.testing <- this.erp.testing[(Time >= time.s & 
                                              Time <= time.e &
                                              Channel %in% chan.subset),]

# Also produce channel-wise columnar tables for some learning algorithms
this.erp.training.channel <- channel.form(this.erp.training,has.dups = TRUE)
this.erp.testing.channel <- channel.form(this.erp.testing, has.dups = TRUE)

```

Since we are looking for the P300, let's reduce the feature space to the interval between `r time.s` and `r time.e`ms. Judging from the plots, the most divergent channels are `r chan.subset`, so we also select only those. Lastly, we split the `Voltage` column into multiple `Channel` columns for compatibility with most of the R machine learning functions.

Let's take a moment to look at the class proportions. With `data.table` we can run a fast query to count the number of `Trial` rows grouped by `Class`, so let's do that for the training set and test set, and print them in a nice table.

```{r subsetting.proportions1, results='asis'}
library(knitr)
kable(this.erp.training.channel[,length(Trial),by=Class])

kable(this.erp.testing.channel[,length(Trial),by=Class])

```

Dividing by the total count, we obtain the proportions:

``` {r subsetting.proportions2, results='asis'}
this.erp.training.channel[,length(Trial),by=Class]$V1/sum(this.erp.training.channel[,length(unique(Trial)),by=Class]$V1)

this.erp.testing.channel[,length(Trial),by=Class]$V1/sum(this.erp.testing.channel[,length(unique(Trial)),by=Class]$V1)
```

Definitely less biased than the testing set.

Since they're both fast, let's begin with LDA and shrinkage LDA (SDA). Since `Trial` and `Time` are unrelated variables, we remove those from consideration. We compute a confusion matrix, as well as the classification accuracy.

``` {r learner.lda}
library(klaR)

lda.model <- lda(Class ~ .-Trial-Time, this.erp.training.channel,
                 prior=c(0.5,0.5))
lda.pred <- predict(lda.model, this.erp.testing.channel)
table(predicted = lda.pred$class, data = this.erp.testing.channel[,Class])
1-sum(lda.pred$class != this.erp.testing.channel[,Class])/length(lda.pred$class)

```


There are some specific quirks with the `sda` function, namely that it takes a matrix as input. However, we have already arranged the table exactly how we need, so all we have to do is change the data type and remove the unrelated columns.

```{r learner.slda}
library(sda)

sda.model <- sda(lambda.freqs = 1, # uniform prior
  as.matrix(this.erp.training.channel[,
                                      !(colnames(this.erp.training.channel) %in% 
                                          c("Class","Time","Trial")),
                                      with=FALSE]),
  this.erp.training.channel[,Class])

sda.pred <- predict(
  sda.model, 
   as.matrix(this.erp.testing.channel[,
                                      !(colnames(this.erp.testing.channel) %in% 
                                          c("Class","Time","Trial")),
                                      with=FALSE]))

table(predicted = sda.pred$class, data = this.erp.testing.channel[,Class])
1-sum(sda.pred$class != this.erp.testing.channel[,Class])/length(sda.pred$class)

```

``` {r learner.logistic, results='asis'}
library(LiblineaR)

class.weights <- c("0"=0.2, "1"=0.8) # class weights

logistic.model <- LiblineaR(wi = class.weights,
     cost = 10,
     type=3, # cross=3,
     data = this.erp.training.channel[,!(colnames(this.erp.testing.channel) %in% 
                    c("Class","Time","Trial")),
                    with=FALSE],
     labels = this.erp.training.channel[,Class])

logistic.pred <- predict(logistic.model, proba=TRUE, # parallelize me
                       newx=this.erp.testing.channel)

table(predicted = logistic.pred$predictions, data = this.erp.testing.channel[,Class])
1-sum(logistic.pred$predictions != this.erp.testing.channel[,Class])/length(logistic.pred$predictions)
```

``` {r learner.libsvm, results='asis', echo=FALSE, eval=FALSE}
library(e1071)

class.weights <- c("-1"=0.2, "1"=0.8) # class weights

libsvm.model <- svm(Class ~ .-Trial-Time, this.erp.training.channel,
                 kernel = "linear",
                 class.weights = class.weights)

libsvm.pred <- predict(libsvm.model, # proba=TRUE # parallelize me
                       newx=this.erp.testing.channel)

table(predicted = libsvm.pred$predictions, data = this.erp.testing.channel[,Class])
1-sum(libsvm.pred$predictions != this.erp.testing.channel[,Class])/length(libsvm.pred$predictions)
```

### Validation

Validation is performed by computing an ROC curve over the posteriors generated by each classifier.

``` {r roc.lda, message=FALSE, results='hide'}
library(pROC)

# Syntax (response, predictor)
lda.roc <- plot.roc(this.erp.testing.channel[,Class],
                    lda.pred$posterior[,1],
                    ci=TRUE, of="se", #specificities = seq(0,100,5),
                    ci.type="shape",
                    ci.col="#1c61b6AA")

```

```{r roc.sda, results='hide'}
sda.roc <- plot.roc(this.erp.testing.channel[,Class],
                    sda.pred$posterior[,1],
                    ci=TRUE, of="se",
                    ci.type="shape",
                    ci.col="#1c61b6AA")

```

```{r roc.logistic, results='hide'}
logistic.roc <- plot.roc(this.erp.testing.channel[,Class],
                    logistic.pred$probabilities[,1],
                    ci=TRUE, of="se",
                    ci.type="shape",
                    ci.col="#1c61b6AA")
```

``` {r slda.auc, results='asis', echo=FALSE, eval=FALSE}
# channel.subset <- c(1,2,10,12,13,14,15,16)
# channel.subset <- c(6,9)
channel.subset <- seq_len(16)
time.subset <- matrix(c(100,150,
                        200,250,
                        250,350,
                        375,450,
                        500,600),
                      ncol=2,byrow=TRUE)

test.auc <- timespace.auc(this.erp.training,this.erp.testing,
                          time.subset=time.subset,
                          channel.subset=channel.subset)
```

``` {r logistic.auc, results='asis', echo=FALSE, eval=FALSE}
# channel.subset <- paste("Ch",seq_len(16),sep="")
channel.subset <- seq_len(16)
time.subset <- matrix(c(100,150,
                        200,250,
                        250,350,
                        375,450,
                        500,600),
                      ncol=2,byrow=TRUE)

test.auc <- timespace.auc(this.erp.training,this.erp.testing,
                          time.subset=time.subset,
                          channel.subset=channel.subset,learner="LiblineaR")
```

``` {r ksvm.auc, results='asis', echo=FALSE, eval=FALSE}
# channel.subset <- paste("Ch",seq_len(16),sep="")
channel.subset <- seq_len(16)
time.subset <- matrix(c(100,150,
                        200,250,
                        250,350,
                        375,450,
                        500,600),
                      ncol=2,byrow=TRUE)

test.auc <- timespace.auc(this.erp.training,this.erp.testing,
                          time.subset=time.subset,
                          channel.subset=channel.subset,learner="ksvm")
```

``` {r learner.kern.grid.manual, results='asis', echo=FALSE, eval=FALSE}
# small.training.set <- training.set[1:10,c("Voltage","Class"),with=FALSE]
# test.classifier <- ksvm(data = small.training.set,
#                         kernel = "vanilladot",
#                         x = Class ~ Voltage)

# figure out a good parameter for sigma
# parallelized
library(doMC)
registerDoMC(3)

library(kernlab)
hyper.errors <- foreach (this.sigma = seq(from = 27, to = 27.5, by = 0.5),
                         .combine = 'cbind') %:%
  foreach (this.cost = seq(from = 10, to = 60, by = 50),
           .combine = 'c') %dopar% {
  ksvm(data = this.erp.training.channel,
       kernel = "rbfdot",
       C = this.cost, kpar = list(sigma=this.sigma),
       x = Class ~ .-Trial)@error
  }



library(knitr)
kable(hyper.errors)
```