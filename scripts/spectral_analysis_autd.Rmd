```{r load.libs, echo=FALSE, message=FALSE}
library(signal) # signal processing
library(ggplot2) # plotting
library(GGally)
library(knitr) # formatting

setwd(project.root)
source('./.Rprofile')
# load unfiltered data
load("./data/AUTD/au/autd.Rdata") 
```

Spectral Analysis of AUTD data
=============

Introduction
-------------

This program loads data from a single subject in the AUTD data, and performs a spectral analysis with some exploratory plots. The data was produced by a P300 speller experiment, in which there were two classes--one class for the null response (no P300 expected) and one for which a P300 response was expected (i.e., the stimulus for that trial contained the desired letter.)

description of experiment

Spectral Information
-------------

### Unfiltered Spectra

The following plots show the unfiltered spectra, separated by class, and colored by channel.

``` {r explore.spectral, warning=FALSE, message=FALSE, error=FALSE, fig.width=12, fig.height=8}
# this.subject = "to"
# erp <- erp[Subject == this.subject,]

#=================== Plot unfiltered spectrum
freq.bins <- 500
erp.trial.mean <- erp[,mean(Voltage),by=c("Sample","Channel","Class")]
setkey(erp.trial.mean,Sample)

spectra.table <- erp.trial.mean[,get.spectrum(V1),by=c("Channel","Class")]
setnames(spectra.table, old=colnames(spectra.table),
         new=c("Channel","Class", "Power"))

# Frequency bins in the transform space are given by the initial expression,
# but the vector must be repeated over the whole table
Frequency <- rep(sample.rate * (seq_len(freq.bins)-1) / freq.bins / 2,
                 length.out=nrow(spectra.table))

spectra.table[,Channel:=as.factor(Channel)]
setkey(spectra.table, Channel)
spectra.table <- cbind(spectra.table,Frequency)
rm(Frequency)


# plot.subtitle <- paste("Subject",subject.ind)
ggplot(spectra.table,
       aes(Frequency,Power)) + geom_line(aes(colour = Channel)) +
  facet_wrap(~ Class, scales="free", ncol=2) +
  ggtitle(bquote("Averaged Spectrum by Channel, Class (Unfiltered)")) +
  xlab("Frequency (Hz)") + xlim(0,10) +
  ylab("Amplitude (dB)") +
  # labs(colour = "Channel") +
  guides(col = guide_legend(nrow = spectra.table[,length(unique(Channel))/2], byrow=TRUE, title = "Channel")) +
  theme(plot.title = element_text(size = 18, face = "bold", colour = "black", 
                                  vjust=1))

# Cleanup
# rm(spectra.table)
```

### Filtering

With the assumption that the P300 response is of extremely low frequency, we design a Type 1 Chebyshov filter with a passband below 5Hz and ripple 0.5dB, with 20dB of stopband attenuation. This filter is then applied to each channel for each trial.

``` {r filtering, warning=FALSE}


# Filter out everything except 0.2-10Hz
# band.pass <- cheby1(cheb1ord(Wp = c(0.2/(sample.rate/2),10/(sample.rate/2)),
#                              Ws = c(0.1/(sample.rate/2),12/(sample.rate/2)),
#                              Rp = 0.5, Rs = 20))

# Filter out everything above 10 Hz
end.f <- 20
start.f <- 10


low.pass <- butter(buttord(Wp = start.f/sample.rate*2,
                           Ws = end.f/sample.rate*2,
                           Rp = 0.5, Rs = 40))

# Plot designed filter
filter.char <- freqz(low.pass, Fs = sample.rate)
filter.char <- data.frame(freq = filter.char$f,
                    mag = 20 * log10( abs(filter.char$h ) ) )
ggplot(filter.char, aes(x = freq, y = mag)) + xlim(0,20) + ylim(-30,5) + 
  geom_line() + 
  ggtitle(bquote("Magnitude Response of Applied Filter")) +
  xlab("Frequency (Hz)") +
  ylab("Amplitude (dB)")


erp.filtered <- copy(erp)
erp.filtered <- erp.filtered[,Voltage:=filtfilt(low.pass,Voltage),
          by=c("Trial","Channel")]
# downsample, since we filtered down
ds.f <- (end.f+1)*2/sample.rate
erp.filtered <- downsample.dt(erp.filtered,ds.factor=ds.f)

setwd("~/unisync/code/002 - bci_master/p300")
save(erp.filtered, 
     file= "./data/AUTD/au/autd_filtered.Rdata")
```

### Filtered Spectra

Plots of the filtered spectra follow.

``` {r explore.spectral.filtered, warning=FALSE, message=FALSE, error=FALSE, fig.width=12, fig.height=8}

#==================== Repeat for filtered table
freq.bins <- 500
erp.filtered.trial.mean <- erp.filtered[,mean(Voltage),
                                        by=c("Sample","Channel","Class")]
setkey(erp.filtered.trial.mean,Sample)

spectra.filtered.table <- erp.filtered.trial.mean[,get.spectrum(V1),
                                         by=c("Channel","Class")]
setnames(spectra.filtered.table, old=colnames(spectra.filtered.table),
         new=c("Channel","Class", "Power"))

# Frequency bins in the transform space are given by the initial expression,
# but the vector must be repeated over the whole table
Frequency <- rep(sample.rate * (seq_len(freq.bins)-1) / freq.bins / 2,
                 length.out=nrow(spectra.filtered.table))

spectra.filtered.table[,Channel:=as.factor(Channel)]
setkey(spectra.filtered.table, Channel)
spectra.filtered.table <- cbind(spectra.filtered.table,Frequency)
rm(Frequency)


# plot.subtitle <- paste("Subject",subject.ind)
ggplot(spectra.filtered.table,
       aes(Frequency,Power)) + geom_line(aes(colour = Channel)) +
  facet_wrap(~ Class, scales="free", ncol=2) +
  
  ggtitle(bquote("Averaged Spectrum by Channel, Class (Filtered)")) +
  xlab("Frequency (Hz)") + xlim(0,10) +
  ylab("Amplitude (dB)") +
  # labs(colour = "Channel") +
  guides(col = guide_legend(nrow = 8, byrow=TRUE, title = "Channel")) +
  theme(plot.title = element_text(size = 18, face = "bold", colour = "black", 
                                  vjust=1))

# Cleanup
# rm(spectra.filtered.table)
```

### Statistical Summary

For reference, we also compute the mean and variance by class and channel for the unfiltered signals. Additionally, we scale and center the signals and verify that this was successful.

#### Unfiltered Signal
``` {r summary.stats.unfiltered, results='asis', echo=FALSE}
setkey(erp,Class)
cond.stats <- as.data.frame(c(erp[,mean(Voltage),by="Class"]$V1,
                    erp[,var(Voltage),by="Class"]$V1,
                    erp[,mean(Voltage)],
                    erp[,var(Voltage)]))
  
rownames(cond.stats) <- c(paste("Mean Class",erp[,unique(Class)]),
                          paste("Variance Class",erp[,unique(Class)]),
                          "Global Mean", "Global Variance")

colnames(cond.stats) <- "Value"

kable(cond.stats)


# Scale parameters
erp <- erp[,Voltage:=scale(Voltage),by=c("Channel")]

# Verify scaling
cond.stats <- as.data.frame(c(erp[,mean(Voltage),by="Class"]$V1,
                    erp[,var(Voltage),by="Class"]$V1,
                    erp[,mean(Voltage)],
                    erp[,var(Voltage)]))
  
rownames(cond.stats) <- c(paste("Mean Class",erp[,unique(Class)]),
                          paste("Variance Class",erp[,unique(Class)]),
                          "Global Mean", "Global Variance")

colnames(cond.stats) <- "Value"

kable(cond.stats)
```

#### Filtered Signal
``` {r summary.stats.filtered, results='asis', echo=FALSE}
setkey(erp.filtered,Class)
cond.stats <- as.data.frame(c(erp.filtered[,mean(Voltage),by="Class"]$V1,
                    erp.filtered[,var(Voltage),by="Class"]$V1,
                    erp.filtered[,mean(Voltage)],
                    erp.filtered[,var(Voltage)]))
  
rownames(cond.stats) <- c(paste("Mean Class",erp.filtered[,unique(Class)]),
                          paste("Variance Class",erp.filtered[,unique(Class)]),
                          "Global Mean", "Global Variance")

colnames(cond.stats) <- "Value"

kable(cond.stats)


# Scale parameters
erp.filtered <- erp.filtered[,Voltage:=scale(Voltage),by=c("Channel")]

# Verify scaling
cond.stats <- as.data.frame(c(erp.filtered[,mean(Voltage),by="Class"]$V1,
                    erp.filtered[,var(Voltage),by="Class"]$V1,
                    erp.filtered[,mean(Voltage)],
                    erp.filtered[,var(Voltage)]))
  
rownames(cond.stats) <- c(paste("Mean Class",erp.filtered[,unique(Class)]),
                          paste("Variance Class",erp.filtered[,unique(Class)]),
                          "Global Mean", "Global Variance")

colnames(cond.stats) <- "Value"

kable(cond.stats)

# Cleanup if necessary to save memory
# rm(cond.stats)
```

Pairwise Plots and Channel Densities
------------

We would like to assess how separable the classes are in the given time domain. As a first step, we sample 10% of the points randomly, and plot them pairwise. Correlations and histograms for each channel pair are also calculated both in-class as well as for the whole set.

#### Unfiltered Signal

``` {r histograms, eval=TRUE, tidy=FALSE, warning=FALSE, echo=FALSE, fig.width=11, fig.height=11}

# Compute empirical densities by channel, grouped by class
# Subsample
sample.fraction <- 0.1/length(unique(erp$Channel))/length(unique(erp$Class))
sample.table <- 
  erp[,
      invisible(sample(Voltage,
                       size = sample.fraction*nrow(erp))),
      by = c("Class", "Channel")]

setkey(sample.table, Class)

chan.split <- split(sample.table,sample.table[,Channel])

chan.d <- cbind(lapply(chan.split, function(x){
  x$V1}))

chan.d <- as.data.table(matrix(unlist(chan.d),
                 ncol = length(unique(erp$Channel)), 
                 byrow=TRUE))
#setnames(chan.d,channel.names)

chan.d <- chan.d[,Class:= chan.split[[1]]$Class]
chan.d[,Class:=as.factor(Class)]

# for readability split the plots are chunked into parts
channel.chunks <- split(seq_len(length(unique(erp$Channel))),1:5)

for (set.ind in seq_len(length(channel.chunks))) {
  #plot.new()
  #theme_set(theme_grey(base_size=2))
  my.plot <- ggpairs(
    chan.d,
    diag=list(continuous="density"),
    lower=list(continuous="points"),
    #upper=list(continuous="density"),
    columns=channel.chunks[[set.ind]],
    colour="Class",
    title = "Unfiltered Pairwise Channel Space (by Class)",
    axisLabels="show", params=list(corSize=0.01))
  print(my.plot)
}

# Cleanup
#rm(my.plot,sample.table,channel.chunks,chan.split,chan.d)
```

As you can see, for the unfiltered data, the signal is very difficult to separate.

#### Filtered Signal

The situation seems to improve slightly for the filtered signals.

``` {r histograms.filtered, eval=TRUE, tidy=FALSE, warning=FALSE, echo=FALSE, fig.width=11, fig.height=11}

# Compute empirical densities by channel, grouped by class
# Subsample
sample.fraction <- 0.01/length(unique(erp.filtered$Channel))/length(unique(erp.filtered$Class))
sample.table <- 
  erp.filtered[,
      invisible(sample(Voltage,
                       size = sample.fraction*nrow(erp.filtered))),
      by = c("Class", "Channel","Time")]

setkey(sample.table, Class)

sample.table.channel <- channel.form(sample.table)

# for readability split the plots are chunked into parts
channel.chunks <- split(seq_len(length(unique(erp.filtered$Channel))),1:5)

for (set.ind in seq_len(length(channel.chunks))) {
  #plot.new()
  #theme_set(theme_grey(base_size=2))
  my.plot <- ggpairs(
    chan.d,
    diag=list(continuous="density"),
    lower=list(continuous="points"),
    #upper=list(continuous="density"),
    columns=channel.chunks[[set.ind]], colour="Class",
    title = "Filtered Pairwise Channel Space (by Class)",
    axisLabels="show", params=list(corSize=0.01))
  print(my.plot)
}

# Cleanup
#rm(my.plot,sample.table,channel.chunks,chan.split,chan.d)
```

Observations
------------

The difference in variance between classes is roughly two orders of magnitude. However, the signal does not appear to be well-separated in any pairwise space.