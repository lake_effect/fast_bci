[//]: # is this even sensical for determining best variables for a classifier? The answer is probably no. CCA is best for determining relationships/redundancies between determining variables; i.e., eliminating variables that carry the same information here, e.g., highly correlated ones. On the other hand, if using every variable doesn't help with classification, why should this, in the absence of some kind of shrinkage/regularization?

```{r load.libs, echo=FALSE, results='hide', message=FALSE}
library(data.table) # efficient data structure
library(reshape2) # for making "long" table form from data
library(ggplot2) # plotting
library(CCA) # canonical correlation analysis
library(RColorBrewer) # colors
library(ellipse) # correlation ellipse plots
library(gplots) # correlation heatmaps (hierarchical)
library(pheatmap) # simpler hierarchical correlation heatmaps

setwd("~/SpiderOak Hive/code/002 - bci_master/p300")
load("./data/AAS010R01_filtered.Rdata") # load filtered data
```

```{r split.channels}
# Subsample
# sample.fraction <- 1/length(unique(erp.filtered$Channel))/length(unique(erp.filtered$Class))
# sample.table <- 
#   erp.filtered[,
#       invisible(sample(Voltage,
#                        size = sample.fraction*nrow(erp.filtered))),
#       by = c("Class", "Channel")]
sample.table <- erp.filtered

setkey(sample.table, Class)

chan.split <- split(sample.table,sample.table[,Channel])

# cut.fringes <- min(sapply(chan.split,function(x) nrow(x)))
chan.d <- cbind(lapply(chan.split, function(x){
  x$Voltage}))

chan.d <- as.data.table(matrix(unlist(chan.d),
                 ncol = length(unique(erp.filtered$Channel)), 
                 byrow=TRUE))
setnames(chan.d, old=colnames(chan.d),
         new=paste("Channel",seq_len(ncol(chan.d)),sep="."))

chan.d <- chan.d[,Class:= chan.split[[1]]$Class]
chan.d <- chan.d[,Trial:= chan.split[[1]]$Trial]
```

```{r corr.mats}
chan.vec <- c(erp.filtered[,unique(Channel)])

# cross-correlation matrices between null class and all others
cross.class.corrs.bin <- matcor(chan.d[Class==0,chan.vec,with=FALSE],
                             chan.d[Class!=0,chan.vec,with=FALSE])

# styling for plots
colors <- c("#A50F15","#DE2D26","#FB6A4A","#FCAE91","#FEE5D9","white",
            "#EFF3FF","#BDD7E7","#6BAED6","#3182BD","#08519C")
ranks <- order(cross.class.corrs.bin$Xcor[1,])
diag <- cross.class.corrs.bin$Xcor[ranks,ranks]
recy.colors <- colors[5*diag + 6]

# autocorrelation of null/non-null matrices
plotcorr(cross.class.corrs.bin$Xcor, col = recy.colors, outline=FALSE)
plotcorr(cross.class.corrs.bin$Ycor, col = recy.colors, outline=FALSE)

ranks <- order(cross.class.corrs.bin$XYcor[1,])
diag <- cross.class.corrs.bin$XYcor[ranks,ranks]
recy.colors <- colors[5*diag + 6]
# crosscorrelation between null/non-null
plotcorr(cross.class.corrs.bin$XYcor, col = recy.colors, outline=FALSE)

# clustered corr mats
heatmap.2(cross.class.corrs.bin$Xcor, 
          scale='none', dendrogram="row", symm = T, 
          col = colorRampPalette(rev(brewer.pal(n = 7, name = "RdYlBu")))(50))
heatmap.2(cross.class.corrs.bin$Ycor, 
          scale='none', dendrogram="row", symm = T, 
          col = colorRampPalette(rev(brewer.pal(n = 7, name = "RdYlBu")))(50))
heatmap.2(cross.class.corrs.bin$XYcor, 
          scale='none', dendrogram="row", symm = T, 
          col = colorRampPalette(rev(brewer.pal(n = 7, name = "RdYlBu")))(50))
```

``` {r cca}
class0.chan <- chan.d[Class==0,chan.vec,with=FALSE]

CCA.bin <- cc(class0.chan[1:nrow(chan.d[Class!=0,chan.vec,with=FALSE]),],
                             chan.d[Class!=0,chan.vec,with=FALSE])

# plot CCA - variables and individuals
plt.cc(CCA.bin)

# annotations
colnames(CCA.bin$scores$corr.X.xscores) <- 
  rownames(CCA.bin$scores$corr.X.xscores)
colnames(CCA.bin$scores$corr.Y.yscores) <- 
  rownames(CCA.bin$scores$corr.Y.yscores)
colnames(CCA.bin$scores$corr.Y.xscores) <- 
  rownames(CCA.bin$scores$corr.Y.xscores)

pheatmap(CCA.bin$scores$corr.X.xscores)
pheatmap(CCA.bin$scores$corr.Y.yscores)
pheatmap(CCA.bin$scores$corr.Y.xscores)

```

Casually thresholding, we can assign the channels to the following groups:

```{r casual.thresholding, echo=FALSE, results='asis'}
casual.threshold <- list(null.low.corr = sort(c(28,11,24,5,26,18,27)), non.null.high.corr = sort(c(10,3,25,12,9,37,26,5,42,2)), cross.high.corr = c(2,3))

casual.threshold$null.high.corr <- setdiff(1:64,casual.threshold$null.low.corr)

row.l <- max(sapply(casual.threshold,length))

casual.threshold <- lapply(casual.threshold,function(x){
  x <- c(x,rep("", row.l - length(x)))
  })

casual.threshold <- as.data.table(casual.threshold)
casual.threshold <- casual.threshold[,c(4,2,3),with=FALSE]

kable(casual.threshold)
```

Removing common elements, we expect the following channels to have good discriminative power:

```{r candidate.channels, echo=FALSE, results='asis'}
cand.ch <- list(
  null.ch = setdiff(setdiff(casual.threshold$null.high.corr,
                             casual.threshold$non.null.high.corr),
                     casual.threshold$cross.high.corr),
  non.null.ch = setdiff(setdiff(casual.threshold$non.null.high.corr,
                                 casual.threshold$null.high.corr),
                         casual.threshold$cross.high.corr))

row.l <- max(sapply(cand.ch,length))

cand.ch <- lapply(cand.ch,function(x){
  x <- c(x,rep("", row.l - length(x)))
  })

cand.ch <- as.data.table(cand.ch)

kable(cand.ch)
```
