```{r load.libs, echo=FALSE, results='hide', message=FALSE}
library(data.table) # efficient data structure
library(reshape2) # for making "long" table form from data
library(ggplot2) # plotting
library(RColorBrewer) # colors
library(pheatmap) # simpler hierarchical correlation heatmaps
library(foreach) # parallel loops

source("../.Rprofile")
setwd(project.root)

# load dummy data
set.seed(115)
this.data <- dummy.plug(trial.count = 200,
                       chan.count = 4,
                       mean.diff = 50,
                       value.name = "Value")
```

``` {r prestim.baseline}
this.data <- baseline.remove(this.data, val.col = "Value")
```

```{r learner.training}
# Relabel classes to {-1, 0}
# this.data[,Class:= as.numeric(Class)]
# this.data[(Class==1),Class:=-1]
# this.data[,Class:= as.factor(Class)]

# Relabel classes to {-1,1}
this.data[,Class:= as.numeric(Class)]
this.data[(Class==0),Class:=-1]
this.data[,Class:= as.factor(Class)]

# Scale features
this.data[,Value:= scale(Value, center=FALSE)]

# Time subset
time.s <- this.data[,min(Time)]
time.e <- this.data[,max(Time)]

# Channel subset
# chan.subset <- c(2,9,12,13,15)
chan.subset <- this.data[,unique(Channel)]

# Training set size
sample.fraction <- 0.7
training.trials <- sample(x=this.data[,unique(Trial)],
                          size = sample.fraction*this.data[,max(unique(Trial))])
this.data.training <- this.data[(Time >= time.s & Time <= time.e & 
                       Trial %in% training.trials & 
                       Channel %in% chan.subset),]
this.data.testing <- this.data[(Time >= time.s & Time <= time.e & 
                      !(Trial %in% training.trials) &
                      Channel %in% chan.subset),]

# Also produce channel-wise columnar tables for some learning algorithms
this.data.training.channel <- dcast.data.table(this.data.training, 
                                               Class+Time+Trial~Channel,
                                               fun.aggregate=identity,
                                               fill=NA,
                                               value.var="Value")

this.data.testing.channel <- dcast.data.table(this.data.testing,
                                              Class+Time+Trial~Channel,
                                              fun.aggregate=identity,
                                              fill=NA,
                                              value.var="Value")
```

``` {r class.avgs}
survey.plot(this.data.training,
            title.s = "Averages by Class, Channel (dummy training set)",
            val.col = "Value")

survey.plot(this.data.testing,
            title.s = "Averages by Class, Channel (dummy testing set)",
            val.col = "Value")
```

``` {r slda.auc, results='asis', eval='false'}
channel.subset <- paste('V',seq_len(4), sep='')
time.subset <- matrix(c(100,150,
                        200,250,
                        250,350,
                        375,450,
                        500,600),
                      ncol=2,byrow=TRUE)

test.auc <- timespace.grid(this.data.training,
                          this.data.testing,
                          time.subset=time.subset,
                          channel.subset=channel.subset,
                          value.col="Value")
```

```{r learner.swlda, results='asis'}
library(klaR)

lda.model <- lda(Class ~ . -Trial, this.data.training.channel)
lda.pred <- predict(lda.model, this.data.testing.channel)
table(predicted = lda.pred$class, data = this.data.testing.channel[,Class])
1-sum(lda.pred$class != this.data.testing.channel[,Class])/length(lda.pred$class)

swlda.model <- 
  stepclass(Class ~ .,
            data = 
              this.data.training.channel[,!(colnames(this.data.training.channel) %in% 
                                             "Trial"),with=FALSE],
                         method = "lda",
                         start.vars = 1:4,
                         output = TRUE,
                         improvement = 0.05)
```