Parallel/memory-efficient approaches to BCI data processing in R
=========

This is a small collection of scripts in R and R Markdown demonstrating
parallelized and/or memory-efficient approaches to BCI data processing, mainly
with the goal of binary classification.

The main results are given in
[a 2014 paper (link broken)](). Unfortunately
the data used cannot be shared, but the code here can provide a starting point
for your own explorations.

## scripts

The main scripts of interest are named and summarized here. The stars of the show are in the benchmarks section.

### benchmarks

Benchmarks were performed using the excellent `microbenchmark` package.

#### filtering

The script [benchmarks/filtfiltbenchmark.R](benchmarks/filtfiltbenchmark.R)
compares the runtime of serial and parallel two-way digital filtering of the
first 100 million rows of experimental data, grouped by experimental trial, for
2, 3, and 4 cores.

#### hyperparameter optimization

The script [benchmarks/hyperoptbenchmark.R](benchmarks/hyperoptbenchmark.R) compares the runtimes of serial and parallel grid search over hyperparameters for linear discriminant analysis with shrinkage (SDA) and linear support vector vector machines (SVM) with L2 regularization and L1 loss.

#### grand means

The script [benchmarks/memorybenchmarks.R](benchmarks/memorybenchmarks.R)
contains a number of benchmarks. Each of them compares a call-by-value operation
with an equivalent call-by-reference operation using `data.table`. The first one
is a comparison of grand means (aggregation) with a group in the query.

#### channel spectra

The second memory benchmark, in the same file, computes the memory cost for
computing the average spectrum (i.e., frequency content) of the data, grouped by
channel and class.

#### scaling signal

Finally, the last benchmark demonstrates a feature common to R and MATLAB,
namely the 'lazy' evaluation that often results in call-by-value behavior in
complex cases. In the case of scaling the value column of a large table so that
its values have mean 0 and variance 1, there is no need to enforce
call-by-reference, since no copying is needed either way.

### data flow ###

Scripts as used to examine the experimental data are given here.

#### import ####

[scripts/datain_autd.R](scripts/datain_autd.R) was used to quickly and
efficiently import 3 trials from 13 subjects, totaling 702Mb on disk in
compressed MATLAB binary format. The data was stored in a structure in MATLAB (a
list as imported in R) as follows:

1. epoched EEG data tensor(3-dimensional array), where the first dimension was
   sample/time index, the second channel index, and the third trial index;
2. Unused here
3. A 1-D vector of booleans indicating the class of each trial.

A (possibly) simpler version that imports only one is at
[scripts/datain_autd_single.R](scripts/datain_autd_single.R).

#### filtering ####

Filtering is one of the most repetitive tasks to program for. There are a couple
scripts that use different methods to do so, on different data sets. The goal
here was to remove high-frequency components from the data which were not of
interest (we were looking for the P300 response, which is relatively 'slow').

- [scripts/spectral_analysis_autd.Rmd](scripts/spectral_analysis_autd.Rmd)
- [scripts/spectral_analysis_bcicomp.Rmd](scripts/spectral_analysis_bcicomp.Rmd)

#### feature selection and classification

Finally, the filtered data undergoes the last few steps:

1. removal of baseline (mean before stimulus at 100ms)
2. scaling
3. subsetting to hold out one experimental session
4. time windowing around area of interest (200-450ms post-stimulus)
5. linear PCA (three components with largest eigenvalues)

The resulting features were classified using the following classifiers, with the
same configuration as in the benchmark:

- LDA
- SDA
- SVM

Each algorithm was first run with default parameters to establish a
ballpark. Following that, a grid search was performed over hyperparameters for
each algorithm
([see paper for details (link broken)]()) to
find the the hyperparameters that yielded the best performance as trained on the
training set, on the testing set.

### miscellaneous ###

#### utility functions ####

A collection of utility functions is in [scripts/funcs.R](scripts/funcs.R). This
may actually be the most useful code in the repo.

#### dummy generator ####

I ran into a wall at one point, and wrote a whole script to debug my thought
process: [scripts/learning_problem.R](scripts/learning_problem.R). The resulting
script may be helpful in conceptualizing the analysis process.
