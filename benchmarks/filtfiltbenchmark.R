library(foreach)
library(iterators)
library(doMC)
library(signal)


setwd(project.root)
source('./.Rprofile')
# load unfiltered data
load("./data/AUTD/au/autd.Rdata") 

setkey(erp,Trial,Sample)

# Filter out everything above 10 Hz
end.f <- 20
start.f <- 10
low.pass <- butter(buttord(Wp = start.f/sample.rate*2,
                           Ws = end.f/sample.rate*2,
                           Rp = 0.5, Rs = 40))

filter.para <- expression(erp[1:100000000,
    foreach(this.trial=iter(erp[,unique(Trial)]),
            .combine=c) %dopar% {
                filtfilt(low.pass,erp[J(this.trial)
                                     ,Voltage])
            }])

filter.seri <- expression(erp[1:100000000,filtfilt(low.pass,Voltage),
    by=c("Trial","Channel")])

### do benchmark
## TODO merge all this into one single benchmark run for easier table handling
cycles <- 20
library(microbenchmark)
registerDoMC(cores=2)
filter.bench.2cores <- microbenchmark(list=c(filter.para,filter.seri),
                                      times=cycles)
registerDoMC(cores=3)
filter.bench.3cores <- microbenchmark(list=c(filter.para,filter.seri),
                                      times=cycles)
registerDoMC(cores=4)
filter.bench.4cores <- microbenchmark(list=c(filter.para,filter.seri),
                                      times=cycles)

## rename expression names for better formatting
new.levels <- as.factor(c("parallel, 2 cores","serial"))
filter.bench.2cores$expr <- new.levels[filter.bench.2cores$expr]
new.levels <- as.factor(c("parallel, 3 cores","serial"))
filter.bench.3cores$expr <- new.levels[filter.bench.3cores$expr]
new.levels <- as.factor(c("parallel, 4 cores","serial"))
filter.bench.4cores$expr <- new.levels[filter.bench.4cores$expr]


## Merge different results into one table
filter.bench <- merge(x=filter.bench.2cores[which(filter.bench.2cores$expr==levels(filter.bench.2cores$expr)[1]),],y=filter.bench.3cores[which(filter.bench.3cores$expr==levels(filter.bench.3cores$expr)[1]),], all=TRUE)
filter.bench <- merge(x=filter.bench,y=filter.bench.4cores[which(filter.bench.4cores$expr==levels(filter.bench.4cores$expr)[1]),], all=TRUE)
filter.bench <- merge(x=filter.bench,y=filter.bench.4cores[which(filter.bench.4cores$expr==levels(filter.bench.4cores$expr)[2]),], all=TRUE)

### plot results
library(ggplot2)
# names(filter.bench.4cores) <- c("Type","Time")
# names(filter.bench.3cores) <- c("Type","Time")
# names(filter.bench.2cores) <- c("Type","Time")
# qplot(y=Time, x=Type, data=filter.bench.2cores, geom="boxplot", 
#       xlab="Type", ylab="Time(s)",
#       fill=Type)
# qplot(y=Time, x=Type, data=filter.bench.3cores, geom="boxplot", 
#       xlab="Type", ylab="Time(s)",
#       fill=Type)
# qplot(y=Time, x=Type, data=filter.bench.4cores, geom="boxplot", 
#       xlab="Type", ylab="Time(s)",
#       fill=Type)

names(filter.bench) <- c("Type","Time")
filter.bench$Type <- relevel(filter.bench$Type, "serial")
qplot(y=Time, x=Type, data=filter.bench, geom="boxplot", 
      xlab="Type", ylab="Time(s)",
      fill=Type)
