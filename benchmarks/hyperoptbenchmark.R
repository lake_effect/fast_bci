library(foreach)
library(iterators)
library(doMC)
library(signal)


setwd(project.root)
source('./.Rprofile')

##### DATA LOAD SECTION #####
this.subject <- "s13"

# # load filtered data
load("./data/AUTD/au/autd_filtered.Rdata")
setkey(erp.filtered,Subject)
this.erp <- erp.filtered[this.subject,]

this.erp <- baseline.remove(this.erp)

# Scale features
this.erp[,Voltage:= scale(Voltage)]

# train on two sessions, test on one
training.sessions <- c(1,2)
testing.sessions <- 3
setkey(this.erp,Session)
this.erp.training <- this.erp[J(as.factor(training.sessions))]
this.erp.testing <- this.erp[J(as.factor(testing.sessions))]

# this.erp.training <- trial.unit.averager(this.erp.training)

trial.sets <- simple.sampler(this.erp.training,equal.class=TRUE,train.size=1)


# Time subset
time.s <- 200
time.e <- 450

# Channel subset
# chan.subset <- c(6,9)
chan.subset <- this.erp[,unique(Channel)]

this.erp.training <- trial.sets$training.table[(Time >= time.s & 
                                                  Time <= time.e &
                                                  Channel %in% chan.subset),]
this.erp.testing <- this.erp.testing[(Time >= time.s & 
                                        Time <= time.e &
                                        Channel %in% chan.subset),]

# Also produce channel-wise columnar tables for some learning algorithms
this.erp.training.channel <- channel.form(this.erp.training,has.dups = TRUE)
this.erp.testing.channel <- channel.form(this.erp.testing, has.dups = TRUE)


training.pca <- prcomp(~ . -Class-Trial-Time, data = this.erp.training.channel,
                       scale=FALSE, tol=sqrt(.Machine$double.eps))

training.pca.data <- as.data.table(predict(training.pca))
training.pca.data[,Class:= this.erp.training.channel[,Class]]
training.pca.data[,Trial:= this.erp.training.channel[,Trial]]

testing.pca.pred <- predict(training.pca, newdata = this.erp.testing.channel)

testing.pca.pred <- as.data.table(testing.pca.pred)
testing.pca.pred[,Class:= this.erp.testing.channel[,Class]]
testing.pca.pred[,Trial:= this.erp.testing.channel[,Trial]]

testing.pca.long <- melt(testing.pca.pred,
                         id.vars=c("Class","Trial"))
setnames(testing.pca.long,old=colnames(testing.pca.long),
         new=c("Class","Trial","PC","value"))



shrinkage.subset <- list(lambda = seq(from=0, to=0.5, by= 0.01),
                         lambda.var = seq(from = 0, to=0.01, by = 0.001))

sda.opt.exp <- expression(
  foreach(this.lambda=iter(shrinkage.subset$lambda),
          .combine=cbind) %:% 
  foreach(this.lambda.var=iter(shrinkage.subset$lambda.var), 
          .combine=rbind) %dopar% {
            #                 print(this.time.interval)
            #                 print(this.chan)
            
            sda.model <- sda(lambda.freqs = 1, # uniform prior
                             verbose=FALSE, # for benchmarking
                             lambda=this.lambda,
                             lambda.var=this.lambda.var,
                             Xtrain=as.matrix(training.pca.data[,1:3,with=FALSE]),
                             L=training.pca.data[,as.factor(Class)])
            
            sda.pred <- predict(verbose=FALSE, # for benchmarking
              sda.model,as.matrix(testing.pca.pred[,1:3,with=FALSE]))
            
            #                 auc(roc(predictions = sda.pred$class,
            #                         labels = testing.pca.pred[,as.factor(Class)]))
            
            acc <- sum(sda.pred$class != testing.pca.pred[,Class])/testing.pca.pred[,length(Class)]
            
            if (acc < 0.5) { acc <- 1 - acc}
            
            acc
          }
  )

# do benchmark
cycles <- 20
library(microbenchmark)

###### SDA section #####
# TODO find a way to combine the two setting lines to avoid having to redefine
exp.core1 <- expression({
  registerDoMC(cores=1)
  foreach(this.lambda=iter(shrinkage.subset$lambda),
          .combine=cbind) %:% 
    foreach(this.lambda.var=iter(shrinkage.subset$lambda.var), 
            .combine=rbind) %dopar% {
              #                 print(this.time.interval)
              #                 print(this.chan)
              
              sda.model <- sda(lambda.freqs = 1, # uniform prior
                               verbose=FALSE, # for benchmarking
                               lambda=this.lambda,
                               lambda.var=this.lambda.var,
                               Xtrain=as.matrix(training.pca.data[,1:3,with=FALSE]),
                               L=training.pca.data[,as.factor(Class)])
              
              sda.pred <- predict(verbose=FALSE, # for benchmarking
                                  sda.model,as.matrix(testing.pca.pred[,1:3,with=FALSE]))
              
              #                 auc(roc(predictions = sda.pred$class,
              #                         labels = testing.pca.pred[,as.factor(Class)]))
              
              acc <- sum(sda.pred$class != testing.pca.pred[,Class])/testing.pca.pred[,length(Class)]
              
              if (acc < 0.5) { acc <- 1 - acc}
              
              acc
            }
})
exp.core2 <- expression({
  registerDoMC(cores=2)
  foreach(this.lambda=iter(shrinkage.subset$lambda),
          .combine=cbind) %:% 
    foreach(this.lambda.var=iter(shrinkage.subset$lambda.var), 
            .combine=rbind) %dopar% {
              #                 print(this.time.interval)
              #                 print(this.chan)
              
              sda.model <- sda(lambda.freqs = 1, # uniform prior
                               verbose=FALSE, # for benchmarking
                               lambda=this.lambda,
                               lambda.var=this.lambda.var,
                               Xtrain=as.matrix(training.pca.data[,1:3,with=FALSE]),
                               L=training.pca.data[,as.factor(Class)])
              
              sda.pred <- predict(verbose=FALSE, # for benchmarking
                                  sda.model,as.matrix(testing.pca.pred[,1:3,with=FALSE]))
              
              #                 auc(roc(predictions = sda.pred$class,
              #                         labels = testing.pca.pred[,as.factor(Class)]))
              
              acc <- sum(sda.pred$class != testing.pca.pred[,Class])/testing.pca.pred[,length(Class)]
              
              if (acc < 0.5) { acc <- 1 - acc}
              
              acc
            }
})
exp.core3 <- expression({
  registerDoMC(cores=3)
  foreach(this.lambda=iter(shrinkage.subset$lambda),
          .combine=cbind) %:% 
    foreach(this.lambda.var=iter(shrinkage.subset$lambda.var), 
            .combine=rbind) %dopar% {
              #                 print(this.time.interval)
              #                 print(this.chan)
              
              sda.model <- sda(lambda.freqs = 1, # uniform prior
                               verbose=FALSE, # for benchmarking
                               lambda=this.lambda,
                               lambda.var=this.lambda.var,
                               Xtrain=as.matrix(training.pca.data[,1:3,with=FALSE]),
                               L=training.pca.data[,as.factor(Class)])
              
              sda.pred <- predict(verbose=FALSE, # for benchmarking
                                  sda.model,as.matrix(testing.pca.pred[,1:3,with=FALSE]))
              
              #                 auc(roc(predictions = sda.pred$class,
              #                         labels = testing.pca.pred[,as.factor(Class)]))
              
              acc <- sum(sda.pred$class != testing.pca.pred[,Class])/testing.pca.pred[,length(Class)]
              
              if (acc < 0.5) { acc <- 1 - acc}
              
              acc
            }
})
exp.core4 <- expression({
  registerDoMC(cores=4)
  foreach(this.lambda=iter(shrinkage.subset$lambda),
          .combine=cbind) %:% 
    foreach(this.lambda.var=iter(shrinkage.subset$lambda.var), 
            .combine=rbind) %dopar% {
              #                 print(this.time.interval)
              #                 print(this.chan)
              
              sda.model <- sda(lambda.freqs = 1, # uniform prior
                               verbose=FALSE, # for benchmarking
                               lambda=this.lambda,
                               lambda.var=this.lambda.var,
                               Xtrain=as.matrix(training.pca.data[,1:3,with=FALSE]),
                               L=training.pca.data[,as.factor(Class)])
              
              sda.pred <- predict(verbose=FALSE, # for benchmarking
                                  sda.model,as.matrix(testing.pca.pred[,1:3,with=FALSE]))
              
              #                 auc(roc(predictions = sda.pred$class,
              #                         labels = testing.pca.pred[,as.factor(Class)]))
              
              acc <- sum(sda.pred$class != testing.pca.pred[,Class])/testing.pca.pred[,length(Class)]
              
              if (acc < 0.5) { acc <- 1 - acc}
              
              acc
            }
})

sda.opt.bench <- microbenchmark(list=c(exp.core1,
                                       exp.core2,
                                       exp.core3,
                                       exp.core4),
                                times=cycles)

new.levels <- as.factor(c("serial","parallel, 2 cores",
                          "parallel, 3 cores",
                          "parallel, 4 cores"))
sda.opt.bench$expr <- new.levels[sda.opt.bench$expr]

names(sda.opt.bench) <- c("Type","Time")

sda.opt.bench$Type <- relevel(sda.opt.bench$Type,"serial")

library(ggplot2)
qplot(y=Time, x=Type, data=sda.opt.bench, geom="boxplot", 
      xlab="Type", ylab="Time(s)",
      fill=Type)


###### LiblineaR section #####
library(LiblineaR)
class.weights <- c("0"=0.4, "1"=0.4)

# costs.subset <- seq(from = 420, to = 580, by = 10)
costs.subset <- seq(from= 400, to = 600, by= 20)


# TODO merge these redundant expressions together also
linsvm.opt.exp <- expression(

    foreach(this.cost=iter(costs.subset),
            .combine=rbind) %:% 
    foreach(this.run=iter(seq_len(10)), .combine=cbind) %dopar% {
      #                 print(this.time.interval)
      #                 print(this.chan)
      
      svm.model <- LiblineaR(wi = class.weights,
                             cost = this.cost,
                             type=3, # cross=3,
                             data = training.pca.data[,1:3,with=FALSE],
                             labels = training.pca.data[,Class])
      
      svm.pred <- predict(svm.model,
                          newx=testing.pca.pred[,1:3,with=FALSE])
      
      acc <- sum(svm.pred$predictions != testing.pca.pred[,Class])/
        length(svm.pred$predictions)
      
      if (acc < 0.5) { acc <- 1 - acc}
      
      acc
    })

exp.core1 <- expression({
  registerDoMC(cores=1)
  foreach(this.cost=iter(costs.subset),
          .combine=rbind) %:% 
    foreach(this.run=iter(seq_len(10)), .combine=cbind) %dopar% {
      #                 print(this.time.interval)
      #                 print(this.chan)
      
      svm.model <- LiblineaR(wi = class.weights,
                             cost = this.cost,
                             type=3, # cross=3,
                             data = training.pca.data[,1:3,with=FALSE],
                             labels = training.pca.data[,Class])
      
      svm.pred <- predict(svm.model,
                          newx=testing.pca.pred[,1:3,with=FALSE])
      
      acc <- sum(svm.pred$predictions != testing.pca.pred[,Class])/
        length(svm.pred$predictions)
      
      if (acc < 0.5) { acc <- 1 - acc}
      
      acc
    }
  })

exp.core2 <- expression({
  registerDoMC(cores=2)
  foreach(this.cost=iter(costs.subset),
          .combine=rbind) %:% 
    foreach(this.run=iter(seq_len(10)), .combine=cbind) %dopar% {
      #                 print(this.time.interval)
      #                 print(this.chan)
      
      svm.model <- LiblineaR(wi = class.weights,
                             cost = this.cost,
                             type=3, # cross=3,
                             data = training.pca.data[,1:3,with=FALSE],
                             labels = training.pca.data[,Class])
      
      svm.pred <- predict(svm.model,
                          newx=testing.pca.pred[,1:3,with=FALSE])
      
      acc <- sum(svm.pred$predictions != testing.pca.pred[,Class])/
        length(svm.pred$predictions)
      
      if (acc < 0.5) { acc <- 1 - acc}
      
      acc
    }
  })

exp.core3 <- expression({
  registerDoMC(cores=3)
  foreach(this.cost=iter(costs.subset),
          .combine=rbind) %:% 
    foreach(this.run=iter(seq_len(10)), .combine=cbind) %dopar% {
      #                 print(this.time.interval)
      #                 print(this.chan)
      
      svm.model <- LiblineaR(wi = class.weights,
                             cost = this.cost,
                             type=3, # cross=3,
                             data = training.pca.data[,1:3,with=FALSE],
                             labels = training.pca.data[,Class])
      
      svm.pred <- predict(svm.model,
                          newx=testing.pca.pred[,1:3,with=FALSE])
      
      acc <- sum(svm.pred$predictions != testing.pca.pred[,Class])/
        length(svm.pred$predictions)
      
      if (acc < 0.5) { acc <- 1 - acc}
      
      acc
    }
  })

exp.core4 <- expression({
  registerDoMC(cores=4)
  foreach(this.cost=iter(costs.subset),
          .combine=rbind) %:% 
    foreach(this.run=iter(seq_len(10)), .combine=cbind) %dopar% {
      #                 print(this.time.interval)
      #                 print(this.chan)
      
      svm.model <- LiblineaR(wi = class.weights,
                             cost = this.cost,
                             type=3, # cross=3,
                             data = training.pca.data[,1:3,with=FALSE],
                             labels = training.pca.data[,Class])
      
      svm.pred <- predict(svm.model,
                          newx=testing.pca.pred[,1:3,with=FALSE])
      
      acc <- sum(svm.pred$predictions != testing.pca.pred[,Class])/
        length(svm.pred$predictions)
      
      if (acc < 0.5) { acc <- 1 - acc}
      
      acc
    }
  })

linsvm.opt.bench <- microbenchmark(list=c(exp.core1,
                                       exp.core2,
                                       exp.core3,
                                       exp.core4),
                                   times=cycles)

new.levels <- as.factor(c("serial","parallel, 2 cores",
                          "parallel, 3 cores",
                          "parallel, 4 cores"))
linsvm.opt.bench$expr <- new.levels[linsvm.opt.bench$expr]

names(linsvm.opt.bench) <- c("Type","Time")

linsvm.opt.bench$Type <- relevel(linsvm.opt.bench$Type,"serial")

library(ggplot2)
qplot(y=Time, x=Type, data=linsvm.opt.bench, geom="boxplot", 
      xlab="Type", ylab="Time(s)",
      fill=Type)